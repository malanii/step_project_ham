let menu = [...$(".tabs-button")];
let content = [...$(".hidden")];
let activeIndex = 0;
$("#tabs-menu").on("click", function (event) {
    $(menu[activeIndex]).removeClass("tabs-button-active");
    $(content[activeIndex]).removeClass("tabs-item-active");

    activeIndex = $.inArray(event.target, menu);

    $(menu[activeIndex]).addClass("tabs-button-active");
    $(content[activeIndex]).addClass("tabs-item-active");
});
